Microsoft Azure Administrator
Notes:
1.	Content Delivery Profile - Origin types:
o	Storage - for azure storage
o	Cloud Service - for Azure Cloud Services
o	WebApp for Azure Web Apps
o	Custom origin - for any other publicly accessible origin web server (hosted in Azure or elsewhere)
Note - there might be questions asking about the origin type for a web service Content Delivery Profile where the audience across the globe can access the content. Both origin type and the origin hostname fields must be completed.
2.	In order to create a custom image, follow the steps below:
o	Deprovision/Deallocate and generalise VMs
o	Create a custom image
o	Create a VM from the custom image
3.	In order to deploy Azure file sync service do the followings:
o	Install Azure File Sync agent on the server
o	Register the server
o	Create the server endpoint
o	Create cloud endpoint

4.	If you have a VM that hosts IDS (Intrusion Detection System) and you would like to enforce all traffic from other VMs go through that host, then you must enable IP Forwarding

